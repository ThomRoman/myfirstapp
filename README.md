# **Learning Android**

## **¿Que es Android?**

Es un sistema operativo pensado para celulares con un kernel basado en linux.

## **Herramientas**

Android Studio

## **Configuracion**

- configuracion -> SDK manager

> Para los developer el numero de la version no es muy importante, lo que si es improtante es la seccion API Level

## **Tipos de aplicaciones**

- WebApp: Aplicacioes web enfocadas a dispositivos moviles, acceso limitado de los recursos del dispositivo.
  - Ventaja : No necesitan aprobacion para publicarse, miltiplataforma
  - Desventajas : menor rendimiento, conexion a internet
- Nativas : Especificas para tipos de dispositvos, se desarrollan en un lenguaje especifico
  - Ventajas : Hacen uso de un SDK (software development kit), mejor aprovechamiento de recursos
  - Desventajas : no es multiplatadorma, mayor esfuerzo, costo y tiempo de desarrollo
- Hibridas : Parte nativa y parte hibrida
  - Ventajas: Acceso a parte del hardware del dispositivo, multiplataforma

## **Estructura de un proyecto**

- app
  - manifest : direcciones donde esta la columna vertebral del proyecto
    - AndroidManifest.xml : configuracion del proyecto
  - java
    - src
      - main
        - javav/com/android/myfirstapp
        - res : resources
          - layout : interfaces de la aplicacion
            - activity_main.xml
          - drawable : se utiliza para las densidades, se borra las densidades que no corresponden a la aplicaciones
          - mipmap : aqui si se almacenas las densidades

## **¿Qué es un activity?**

Es la represnetacion de una pantalla, esta esta conformada por dos partes una lógica y otra gráfica

> Gradle y Maven sirven para la administracion de dependencias

## **LinearLayout y RelativeLayout**

- Views : Es un objeto que sirve para dibujar algo en pantalla, con el que el usuario puede interactuar
- ViewGroups : Un objeto que se compone de otros views ordenados,dando como resultado un layout.
- LinearLayout : Es un ViewGroup que alinea todos los hijos en una sola direccion vertical y horizontal.
    Crea un scrollbar si el tamaño de la ventana excede el tamaño de la pantalla.
- RelativeLayout : Es un ViewGroup que alinea todos los elementos hijos con posiciones relativas.
    permite especificar la ubicacion de los objetos hijos en relacion a cada uno o a su padre.
- width y height :
    - wrap_content : tamaño depende del contenido, si sobrepasa salta
    - match_content : ocupa todo su ancho o alto que el padre, si sobrepasa lo desborda
    - dp

## **Otros conceptos**

- Toast : mandar una notificación al usuario de la aplicación, es una notificacion emergente
- Crear Activity
- ConstraintLayout
- Modos
    - modo diseño : como lo ve el usuario final
    - modo blueprint : es el esqueleto del modo diseño, solo lo ve el developer en android studio
- Intent : invocar componentes o activities
- RadioGroup : conjunto de radiobutton para que no se selecciones dos radiobutton a la ves
- RadioButton : para poder seleccionar o no seleccionar un campo, true o false
- checkbox : seleccion multiple
- Control Spinner
- ListView
- Almacenamiento de datos
    - SharedPreferences(¿cache de la aplicacion?) : es una clase para almacenar informacion del usuario,
    es recomendado utilizar esta clase cuando se tiene que almacenar algo limitado
    - almacenamiento en ficheros, lectura y escritura
        - los ficheros almacenados solo son accesibles para la aplicacion que los creo,
          no pueden ser leidos por otras aplicaciones, no siquiera por el usuario del telefono
          o dispositivo android.
- TableLayout : Permite agrupar a los componentes en filas y columnas. Tambien contiene un conjunto de componentes
                de tipo TableRow que es el que agrupa componentes visuales por cada fila.
                A diferencia de LinearLayout, en TableLayout se añade una nueva dimension con el cual se pueden
                colocar nuevos componentes tanto verticalmente, asi como horizontalmente en forma de tabla.

                TableLayout no tiene una estructura de tabla. en realidad TableLayout es un LinearLAyout en forma vertical, pero
                con un comportamiento particular. Un TableRow es una especie de Linear LAyout Horizontal

                layout_span en TableRow

```java
package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);// enlaza con su interfaz gráfica xml
        Toast.makeText(this, "OnCreate", Toast.LENGTH_SHORT).show();
        // La actividad está creada
    }
    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "OnStart", Toast.LENGTH_SHORT).show();
        // La actividad está a punto de hacerse visible.
    }
    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "OnResume", Toast.LENGTH_SHORT).show();
        // La actividad se ha vuelto visible (ahora se "reanuda").
    }
    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "OnPause", Toast.LENGTH_SHORT).show();
        // Enfocarse en otra actividad  (esta actividad está a punto de ser "detenida").
    }
    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show();
        // La actividad ya no es visible (ahora está "detenida")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_SHORT).show();
        // La actividad está a punto de ser destruida.
    }
}
```

![alt text](img-capture/ciclo.png)

## **Referencias**

- https://developer.android.com/
- https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_Android
- https://developer.android.com/guide/components/activities/intro-activities?hl=es
- https://developer.android.com/jetpack/guide?hl=es
- https://desarrolloweb.com/articulos/android-que-es-una-activity-o-actividad.html
- https://dotnetlifestyle.wordpress.com/2015/02/26/android-diferencia-entre-linearlayout-y-relativelayout/
- https://codearmy.co/android-relative-vs-constraint-layouts-cual-es-mejor-y-como-usarlo-95c08582ab2e
- https://medium.com/@diegop88/constraint-layout-en-modo-experto-6856db3d2ee4
- https://elbauldelprogramador.com/eliminar-la-pila-de-actividades-back-stack-en-android/
- https://elbauldelprogramador.com/programacion-android-intents-conceptos/
- https://elbauldelprogramador.com/programacion-android-trabajar-con/
- https://elbauldelprogramador.com/fundamentos-programacion-android_17/
- https://ed.team/blog/como-consumir-una-api-rest-desde-android
- https://es.stackoverflow.com/questions/36670/diferencia-entre-fragment-y-fragmentactivity/36678
- https://academiaandroid.com/activity-y-fragments/
- https://stackoverflow.com/questions/3687315/how-to-delete-shared-preferences-data-from-app-in-android
- https://developer.android.com/guide/topics/text/autofill-optimize
- https://academiaandroid.com/activity-y-fragments/
- https://es.stackoverflow.com/questions/36670/diferencia-entre-fragment-y-fragmentactivity/36678
