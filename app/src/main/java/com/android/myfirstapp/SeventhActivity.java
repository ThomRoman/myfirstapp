package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SeventhActivity extends AppCompatActivity {
    Button btn_guardar;
    Button btn_buscar;
    Button btn_limpiar;

    EditText et_usuario;
    EditText mt_informacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seventh);

        btn_buscar = findViewById(R.id.btn_buscar);
        btn_guardar = findViewById(R.id.btn_guardar);
        btn_limpiar = findViewById(R.id.btn_limpiar);

        et_usuario = findViewById(R.id.et_usuario);
        mt_informacion = findViewById(R.id.mt_informacion);


    }
    public void goFirstActivity(View view){
        Intent intent = new Intent (view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    public void buscar(View view){
        String usuario = et_usuario.getText().toString().trim();

        if( usuario.length()<=0 ) {
            Toast.makeText(getApplicationContext(),"Ingrese datos",Toast.LENGTH_LONG).show();
            return;
        }

        SharedPreferences preferences = getSharedPreferences("usuarios", Context.MODE_PRIVATE);
        String informacion = preferences.getString(usuario,"");
        if(informacion.length()<=0){
            Toast.makeText(getApplicationContext(),"Usuario "+usuario+ " no encontrado",Toast.LENGTH_LONG).show();
            return;
        }
        mt_informacion.setText(informacion);
    }
    public void limpiar(View view){
        et_usuario.setText("");
        mt_informacion.setText("");
        SharedPreferences preferences = getSharedPreferences("usuarios", 0);
        preferences.edit().clear().commit();
    }
    public void guardar(View view){
        String usuario = et_usuario.getText().toString().trim();
        String informacion = mt_informacion.getText().toString().trim();

        if( usuario.length()<=0 || informacion.length()<=0 ){
            Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_LONG).show();
            return;
        }
        SharedPreferences preferences = getSharedPreferences("usuarios", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(usuario,informacion).commit();
        Toast.makeText(getApplicationContext(),"Dato Almacenado",Toast.LENGTH_LONG).show();
    }
}