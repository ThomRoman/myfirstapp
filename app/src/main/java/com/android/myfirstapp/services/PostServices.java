package com.android.myfirstapp.services;

import com.android.myfirstapp.model.PostEntity;

import java.util.List;

import retrofit2.Call;

import retrofit2.http.GET;

public interface PostServices {
    String API_ROUTE = "/posts";

    @GET(API_ROUTE)
    Call<List<PostEntity> > getPost();
}
