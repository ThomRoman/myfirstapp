package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.myfirstapp.model.PostEntity;
import com.android.myfirstapp.services.PostServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FifthActivity extends AppCompatActivity {
    private Button irPrimero;
    private ListView listaPost;
    private ImageButton imageButton;
    private List<String> titles = new ArrayList<String>();
    private ArrayAdapter arrayAdapter;
    private List<PostEntity> posts=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        Toast.makeText(getApplicationContext(),"Bienvenido "+getIntent().getStringExtra("nombre"),Toast.LENGTH_LONG).show();
        this.irPrimero = (Button) findViewById(R.id.button5);
        this.listaPost = (ListView) findViewById(R.id.lw_lista_post);
        arrayAdapter = new ArrayAdapter<>(this,R.layout.listview_item_myfirstapp,titles);
        getPosts();
        this.irPrimero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, 0);
            }
        });
        listaPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),posts.get(i).getBody(),Toast.LENGTH_SHORT).show();
            }
        });
        imageButton = findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getPosts() {
        // equivalente al fetch
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PostServices postService = retrofit.create(PostServices.class);

        Call< List<PostEntity> > call = postService.getPost();

        call.enqueue(new Callback<List<PostEntity>>() {
            @Override
            public void onResponse(Call<List<PostEntity>> call, Response<List<PostEntity>> response) {

                for(PostEntity post : response.body()) {
                    titles.add(post.getTitle());
                    posts.add(post);
                }

                arrayAdapter.notifyDataSetChanged();
                listaPost.setAdapter(arrayAdapter);

            }
            @Override
            public void onFailure(Call<List<PostEntity>> call, Throwable t) {
            }

        });

    }
}