package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends AppCompatActivity {
    WebView wv_container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        String url = getIntent().getStringExtra("url");
        wv_container = findViewById(R.id.wv_container);
        wv_container.setWebViewClient(new MyBrowser());
//        wv_container.loadUrl("http://"+url);
        wv_container.getSettings().setLoadsImagesAutomatically(true);
        wv_container.getSettings().setJavaScriptEnabled(true);
        wv_container.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv_container.loadUrl("http://"+url);
    }


    public void cerrar(View view){
        Intent intent = new Intent (view.getContext(), SixthActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }
    private class MyBrowser extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}