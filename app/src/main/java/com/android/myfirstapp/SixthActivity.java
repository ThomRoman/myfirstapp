package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SixthActivity extends AppCompatActivity {
    private Button btnIr;
    private EditText et_url;

    private EditText et_email;
    private Button btn_save;

    private Button btn_limpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);
        this.btnIr = findViewById(R.id.btnIr);
        this.et_url = findViewById(R.id.et_url);
        this.btn_save = findViewById(R.id.btn_save);
        this.et_email = findViewById(R.id.et_email);
        this.btn_limpiar = findViewById(R.id.btn_limpiar);
        SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        this.et_email.setText(preferences.getString("email",""));

        this.btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = et_url.getText().toString().trim();
                if(url.length()<=0){
                    Toast.makeText(getApplicationContext(),"Ingresa una url",Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(view.getContext(),WebViewActivity.class);
                intent.putExtra("url",url);
                startActivityForResult(intent, 0);

            }
        });

        this.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = et_email.getText().toString().trim();
                if(email.length()<=0){
                    Toast.makeText(getApplicationContext(),"Ingresa un email",Toast.LENGTH_SHORT).show();
                    return;
                }
                SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("email",email);
                editor.commit();
//                finish(); // cierra el activity
            }
        });

        this.btn_limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_email.setText("");
                SharedPreferences preferences = getSharedPreferences("datos", 0);
                preferences.edit().clear().commit();
            }
        });
    }
}