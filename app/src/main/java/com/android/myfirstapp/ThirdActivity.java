package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
    }

    public void goFirstActivity(View view){
        Intent intent = new Intent (view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }


    public void generarPromedio(View view){
        String irNumber = ((EditText)findViewById(R.id.irnumber)).getText().toString();
        String micronumber = ((EditText)findViewById(R.id.micronumber)).getText().toString();
        String lpnumber = ((EditText)findViewById(R.id.lpnumber)).getText().toString();

        if(irNumber.length() <=0 || micronumber.length() <=0 || lpnumber.length()<=0){
            Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_LONG).show();
            return;
        }
        int promedio = (Integer.parseInt(irNumber) + Integer.parseInt(micronumber) + Integer.parseInt(lpnumber)) / 3;

        TextView status = findViewById(R.id.idstatus);
        if(promedio<=10){
            status.setText("Estudiante Desaprobado con "+promedio);
            return;
        }
        status.setText("Estudiante Aprobado con "+promedio);
    }
}