package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class EighthActivity extends AppCompatActivity {
    private EditText mt_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eighth);

        mt_text = findViewById(R.id.mt_text);
//        Devuelve una matriz de cadenas que nombran los archivos privados asociados con el paquete de aplicación de este contexto.
        String archivos[] = fileList(); // le descimos a la app que busque todos los ficheros que creo
        // y nos devuelve todos los nombres


        if(!ArchivoExiste(archivos,"bitacora.txt")){
            return;
        }
        try {
            InputStreamReader archivo = new InputStreamReader(openFileInput("bitacora.txt"));
            BufferedReader br = new BufferedReader(archivo); // este variable contrendra toda la info por linea
            String linea = br.readLine();
            String bitacoraCompleta = "";

            while(linea != null){
                bitacoraCompleta = bitacoraCompleta + linea + "\n";
                linea = br.readLine();
            }
            br.close();
            archivo.close();
            mt_text.setText(bitacoraCompleta);
        }catch (IOException e){

        }

    }
    private boolean ArchivoExiste(String archivos [], String NombreArchivo){
        for(int i = 0; i < archivos.length; i++)
            if(NombreArchivo.equals(archivos[i]))
                return true;
        return false;
    }

    public void Guardar(View view){
        try {
            OutputStreamWriter archivo = new OutputStreamWriter(openFileOutput("bitacora.txt", Activity.MODE_PRIVATE));
            archivo.write(mt_text.getText().toString());
            archivo.flush();
            archivo.close();
        }catch (IOException e){

        }
        Toast.makeText(this, "Bitacora guardada correctamente", Toast.LENGTH_SHORT).show();
    }
}