package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class FourthActivity extends AppCompatActivity {
    private EditText editTextNumber1;
    private EditText editTextNumber2;
    private RadioButton rb1,rb2;
    private CheckBox cbxSumar,cbxRestar;
    private Spinner spnListaAcciones;
    private String[] opciones = {"Dividir","multiplicar"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);

        editTextNumber1 = findViewById(R.id.editTextNumber1);
        editTextNumber2 = findViewById(R.id.editTextNumber2);
        rb1 = findViewById(R.id.radioButtonSumar);
        rb2 = findViewById(R.id.radioButtonRestar);
        cbxRestar = findViewById(R.id.cbxRestar);
        cbxSumar = findViewById(R.id.cbxSumar);
        spnListaAcciones = findViewById(R.id.spnListaAcciones);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item,opciones);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_myfirstapp,opciones);
        spnListaAcciones.setAdapter(adapter);
    }
    public void goFirstActivity(View view){
        Intent intent = new Intent (view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    public void calcular(View view){
        String num1 = editTextNumber1.getText().toString().trim();
        String num2 = editTextNumber2.getText().toString().trim();
        if(num1.length()<=0 || num2.length()<=0){
            Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_LONG).show();
            return;
        }
        if((!rb1.isChecked() && !rb2.isChecked() ) || (!cbxRestar.isChecked() && !cbxSumar.isChecked())){
            Toast.makeText(getApplicationContext(),"Seleccione una accion",Toast.LENGTH_LONG).show();
            return;
        }

        switch (spnListaAcciones.getSelectedItem().toString()){
            case "Dividir":
                if(Integer.parseInt(num2) == 0){
                    Toast.makeText(getApplicationContext(),"El denominador no puede ser cero, o escoja otra opcion en el spinner",Toast.LENGTH_LONG).show();
                    return;
                }
                int division = Integer.parseInt(num1) / Integer.parseInt(num2);
                Toast.makeText(getApplicationContext(),"Spinner: La division es "+division,Toast.LENGTH_LONG).show();
                break;
            case "multiplicar":
                int multi = Integer.parseInt(num1) * Integer.parseInt(num2);
                Toast.makeText(getApplicationContext(),"Spinner: La multiplicacion es "+multi,Toast.LENGTH_LONG).show();
                break;
        }

        int suma = Integer.parseInt(num1) + Integer.parseInt(num2);
        int resta = Integer.parseInt(num1) - Integer.parseInt(num2);
        if(rb1.isChecked()){
            Toast.makeText(getApplicationContext(),"Radio : La suma es "+suma ,Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(),"Radio : La resta es "+resta ,Toast.LENGTH_LONG).show();
        }

        if(cbxSumar.isChecked() && cbxRestar.isChecked()){
            Toast.makeText(getApplicationContext(),"Checkbox : La suma es "+suma+", y la resta es "+resta ,Toast.LENGTH_LONG).show();
            return;
        }
        if(cbxRestar.isChecked()){
            Toast.makeText(getApplicationContext(),"Checkbox : La resta es "+resta ,Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(getApplicationContext(),"Checkbox : La suma es "+suma ,Toast.LENGTH_LONG).show();
    }

}