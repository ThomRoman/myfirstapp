package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    EditText editText;
    Button button;
    Button irSegundo;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textview1);
        textView.setText("Inserte su nombre");

        editText = findViewById(R.id.idname);
        button = findViewById(R.id.idbutton);
        message = findViewById(R.id.idmessage);
        irSegundo = findViewById(R.id.idirSegundo);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().toString().trim().length()<=0){
                    Toast.makeText(getApplicationContext(),"Almacena algo xfas",Toast.LENGTH_LONG).show();
                    return;
                }
                String msg = "Su nombre "+editText.getText().toString()+" se guardo exitosamente";
//                message.setText(msg);
                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
            }
        });

        irSegundo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), SecondActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, 0);
            }
        });
    }


    public void goThirdActivity(View view){
        Intent intent = new Intent (view.getContext(), ThirdActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    public void goFourthActivity(View view){
        Intent intent = new Intent (view.getContext(), FourthActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }
    public void goSixthActivity(View view){
        Intent intent = new Intent (view.getContext(), SixthActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }
    public void goSeventhActivity(View view){
        Intent intent = new Intent (view.getContext(), SeventhActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    public void goEighthActivity(View view){
        Intent intent = new Intent (view.getContext(), EighthActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }
    public void goUltimo(View view){
        Intent intent = new Intent (view.getContext(), UltimoActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    public void goFifthActivity(View view){
        if(editText.getText().toString().trim().length()<=0){
            Toast.makeText(getApplicationContext(),"Almacena algo xfas",Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent (view.getContext(), FifthActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("nombre",editText.getText().toString());
        startActivityForResult(intent, 0);
    }

}