package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    Button irMain;
    TextView firstNumber;
    TextView secondNumber;
    Button btnSumar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);// enlaza con su interfaz gráfica xml
        irMain = findViewById(R.id.irMain);
        firstNumber = findViewById(R.id.firstnumber);
        secondNumber = findViewById(R.id.secondnumber);
        btnSumar = findViewById(R.id.btnSumar);
        irMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, 0);
            }
        });
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(firstNumber.getText().toString().trim().length() <=0 || secondNumber.getText().toString().trim().length() <=0){
                    Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_LONG).show();
                    return;
                }
                int numbera = Integer.parseInt(firstNumber.getText().toString());
                int numberb = Integer.parseInt(secondNumber.getText().toString());

                int suma = numbera + numberb;
                Toast.makeText(getApplicationContext(),"La suma es "+Integer.toString(suma),Toast.LENGTH_LONG).show();
            }
        });
    }
}