package com.android.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class UltimoActivity extends AppCompatActivity {
    EditText et_nombre_file;
    EditText et_info;
    Button btn_guardar_sd;
    Button btn_consultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ultimo);
        et_nombre_file = findViewById(R.id.et_name_file);
        et_info = findViewById(R.id.ml_info);
        btn_consultar = findViewById(R.id.btn_guardar_sd);
        btn_consultar = findViewById(R.id.btn_consultar_sd);
    }
    //Método para el botón Guardar
    public void Guardar(View view){
        String nombre = et_nombre_file.getText().toString();
        String contenido = et_info.getText().toString();

        try {
            // obtengo la instancia de la memoria SF
            File tarjetaSD = Environment.getExternalStorageDirectory();

            // muestro con una notificacion emergente la direccion
            Toast.makeText(this, tarjetaSD.getPath(), Toast.LENGTH_SHORT).show();

            // creo un archivo en la direccion que obtuve
            File rutaArchivo = new File(tarjetaSD.getPath(), nombre);

            // ingreso el contenido al archivo
            OutputStreamWriter crearArchivo = new OutputStreamWriter(openFileOutput(nombre, Activity.MODE_PRIVATE));
            crearArchivo.write(contenido);
            crearArchivo.flush();
            crearArchivo.close();

            Toast.makeText(this, "Guardado correctamente", Toast.LENGTH_SHORT).show();
            et_nombre_file.setText("");
            et_info.setText("");
        }catch (IOException e){
            Toast.makeText(this, "No se pudo guardar", Toast.LENGTH_SHORT).show();
        }
    }

    //Método consultar
    public void Consultar(View view){
        String nombre = et_nombre_file.getText().toString();


        try{
            File tarjetaSD = Environment.getExternalStorageDirectory();
            File rutaArchivo = new File(tarjetaSD.getPath(), nombre);
            InputStreamReader abrirArchivo = new InputStreamReader(openFileInput(nombre));

            BufferedReader leerArchivo = new BufferedReader(abrirArchivo);
            String linea = leerArchivo.readLine();
            String contenidoCompleto = "";

            while (linea != null){
                contenidoCompleto = contenidoCompleto + linea + "\n";
                linea = leerArchivo.readLine();
            }

            leerArchivo.close();
            abrirArchivo.close();
            et_info.setText(contenidoCompleto);
        }catch (IOException e){
            Toast.makeText(this, "Error al leer el archivo", Toast.LENGTH_SHORT).show();
        }
    }
}